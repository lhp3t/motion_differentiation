# Learning-based Video Motion Magnification

Tensorflow implementation of Learning-based Video Motion Magnification. [\[Project page\]](https://people.csail.mit.edu/tiam/deepmag/) [\[Paper\]](https://arxiv.org/abs/1804.02684) [\[Data\]](https://groups.csail.mit.edu/graphics/deep_motion_mag/data/readme.txt)

Collaborators: 
\*(Tae-Hyun Oh, Ronnachai "Tiam" Jaroensri), Changil Kim, Mohamed A. Elgharib, Fr&eacute;do Durand, William T. Freeman, Wojciech Matusik

\*Equal contribution.

# Prerequisite
- CUDA and Cudnn
- avconv or ffmpeg
- python3 and pip


# Installation


Create a virtual environment:
```
virtualenv <name>
```
Clone this repo into that folder
```
git clone https://gitlab.com/lhp3t/motion_differentiation.git <name> && cd ./<name>
```
Activate that virtual env:
```
source ./bin/activate
```
Install required packages:

```
pip install -r requirements.txt
```

This code has been tested with Tensorflow 1.3 and 1.8, CUDA 8.5 and 9.1, Ubuntu 14.04 and 16.04, although we expect it to work with any newer versions of Tensorflow and CUDA.
(TTT - worked with Ubuntu 18.04 and 18.10, CUDA 10.0)


# Running

Place video frames under `data/vids/<video_name>`. This script runs wrt image frames, thus you need to extract such video into singular separated images, using either:
```
avconv -i <path_to_input>/<vid>.mp4 -f image2 <path_to_output>/<vid>/%06d.png
```
or alternatively if ur using ffmpeg instead of avconv:
```
ffmpeg -i <path_to_input>/<vid>.mp4 <path_to_output>	# (preferably being put under /data/vids/<name>, otherwise u need to change the config inside the bash script)
```
NOTE: You ll need to change the corresponding method in `magnet.py`, currently it's ffmpeg

To run the script:
```
# Two-frame based processing (Static, Dynamic mode)
sh run_on_test_videos.sh <experiment_name> <video_name> <amplification_factor> <run_dynamic_mode>
# Temporal filtering based processing (frequency band pass filter)
sh run_temporal_on_test_videos.sh <experiment_name> <video_name> <amplification_factor> <low_cutoff> <high_cutoff> <sampling_rate (fps)> <n_filter_tap> <filter_type>
```

Here, Dynamic mode stands for consequtive frame processing.
The code supports `fir | butter | differenceOfIIR` filters. For FIR filters, the filtering could be done both in Fourier domain and in temporal domain.
For example in the `baby` video:
```
# Static mode, using first frame as reference.
sh run_on_test_videos.sh o3f_hmhm2_bg_qnoise_mix4_nl_n_t_ds3 baby 10
# Dynamic mode, magnify difference between consecutive frames.
sh run_on_test_videos.sh o3f_hmhm2_bg_qnoise_mix4_nl_n_t_ds3 baby 10 yes
# Using temporal filter (same as Wadhawa et al.)
sh run_temporal_on_test_videos.sh o3f_hmhm2_bg_qnoise_mix4_nl_n_t_ds3 baby 20 0.04 0.4 30 2 differenceOfIIR
```


For custom video and output location, you should look into modifying the two scripts directly. For custom checkpoint location, look in the configuration file under `configs/`. There should be an option for checkpoint location that you can edit.


# Result - TTT
Some sample outputs were generated using the 2 sample videos and put under `/data/output`
--TO-DO: Evaluate | Thoughts | Open questions


# Acknowledgement

The authors would like to thank Toyota Research Institute, Shell Research, and Qatar Computing Research Institute for their generous support of this project. Changil Kim was supported by a Swiss National Science Foundation fellowship P2EZP2 168785.

Utilities and skeleton code borrowed from [tensorflow implementation of CycleGAN](https://github.com/xhujoy/CycleGAN-tensorflow)
